import React, { useContext, useEffect, useState } from 'react'
import Header from '../UI/Header'
import Link from 'next/link'
import axios from 'axios'
import { useRouter } from 'next/router'
import AuthContext from '../store/AuthContext'

const Signup = () => {
  const authCtx=useContext(AuthContext);
  const router=useRouter();
  const [user,setUser]=useState({
    firstName:'',
    lastName:'',
    username:'',
    password:''
  })
  useEffect(() => {
    if(authCtx.isLoggedIn)
    {
      alert("ALREADY LOGGED IN");
    router.push('/')
    }

  }, [])

  const onSubmitHandler=async()=>{
    if(user.firstName && user.lastName && user.username && user.password){
      try{
        const response=await axios.post('/api/signup',user);
        console.log(response);
        alert(response.data.message)
        router.push('/login')
      }
      catch(err){
        alert(err.response.data.message)
        console.log(err)

      }
      finally{
        setUser({
          firstName:'',
          lastName:'',
          username:'',
          password:''
        });
      }
    }
    else{
      alert('Please fill all the fields')
    }

  }
  return (
    <div className='h-[100vh]'>
      <Header/>
      <div className='h-full flex flex-col justify-center items-center bg-purple-500 p-4'>
        <div className='bg-white py-10 px-8 w-full md:w-[400px] rounded-xl flex flex-col gap-2'>
          <h4 className='text-lg font-bold text-center'>SIGNUP</h4>

          <div className='flex flex-col gap-1'>
            <label>FIRST NAME</label>
            <input value={user.firstName} onChange={(e)=>setUser({...user,firstName:e.target.value})}  type='text' className='bg-blue-100 p-2 rounded-lg' placeholder='alex'/>
          </div>

          <div className='flex flex-col gap-1'>
            <label>LAST NAME</label>
            <input value={user.lastName} onChange={(e)=>setUser({...user,lastName:e.target.value})}  type='text' className='bg-blue-100 p-2 rounded-lg' placeholder='jersey'/>
          </div>

          <div className='flex flex-col gap-1'>
            <label>USERNAME</label>
            <input value={user.username}  onChange={(e)=>setUser({...user,username:e.target.value})} type='text' className='bg-blue-100 p-2 rounded-lg' placeholder='username'/>
          </div>

          <div className='flex flex-col gap-1'>
            <label>PASSWORD</label>
            <input value={user.password} onChange={(e)=>setUser({...user,password:e.target.value})}  type='password' className='bg-blue-100 p-2 rounded-lg' placeholder='password'/>
          </div>
          
          <div>
          <button onClick={onSubmitHandler} className='bg-purple-500 text-white w-full p-2'>SIGNUP</button>
          </div>

          <p>Already have an account ? <Link className='text-blue-500' href={'/login'}>LOGIN</Link></p>

        </div>

      </div>
    </div>
  )
}

export default Signup