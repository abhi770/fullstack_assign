import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import AuthContext from '../store/AuthContext'
import Header from '../UI/Header';
import { useRouter } from 'next/router';

const Orders = () => {
    const authCtx=useContext(AuthContext);
    const [user,setUser]=useState({});
    const [orders,setOrders]=useState([]);
    const [loading,setLoading]=useState([]);
    const [changed,setChanged]=useState(true);
    const router=useRouter();
    useEffect(() => {

        if(!authCtx.isLoggedIn){
            router.push('/login');
            return
        }
        const fetchUserDetails=async()=>{
            try{
            const response=await axios.post('/api/user',{username:authCtx.localid});
            console.log(response.data)
            // console.log(response.data.data)
            setUser(response.data.data);
            fetchOrder(response.data.data.orders)
            }
            catch(err){
                console.log(err)
            }
        }

        const fetchOrder=async(orders)=>{
            const results = [];
            for (const id of orders) {
              try {
                const response = await axios.post('/api/getOrder',{id:id});
                console.log(response.data.data)
                results.push(response.data.data)
              } catch (error) {
                console.error(`Error fetching order with ID ${id}`, error);
              }
              
            }
            setOrders(results)
            setLoading(false);
        }

        
        if(authCtx.isLoggedIn)fetchUserDetails()
    }, [authCtx,changed])

    const cancelOrder=async(book)=>{
        try{
            const response = await axios.post('/api/cancelOrder',{book:book,username:authCtx.localid});
            console.log(response.data.message)
            setChanged(!changed)
        }catch(err){

        }
    }
    
  return (
    <div>
        <Header/>
        <div className='p-4 flex flex-col gap-4'>
            <div className='flex justify-end'>
            {user?.credit && <h2 className='bg-purple-500 text-white p-2'>COINS : {user?.credit}</h2>}
            </div>

            <div className='flex flex-col'>
                {orders.map((item,i)=>
                <div key={i} className=' bg-purple-500 p-8 rounded-lg text-white flex flex-col gap-2 my-4'>
                    <p>TITLE : {item.title}</p>
                    <p>PRICE : {item.price}</p>
                    <button onClick={()=>cancelOrder(item)} className='bg-red-500 p-2 self-end'>CANCEL</button>
                </div>
                )}

                {loading && <h2 className='text-center'>LOADING</h2>}
                {!loading && orders.length===0 && <h2 className='text-center'>NO ORDERS FOUND</h2>}
            </div>
        </div>
    </div>
  )
}

export default Orders