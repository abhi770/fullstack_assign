import React, { useContext, useEffect, useState } from 'react'
import Header from '../UI/Header'
import axios from 'axios';
import Image from 'next/image';
import AuthContext from '../store/AuthContext';
import { useRouter } from 'next/router';

export const Home = () => {

    const [loading,setLoading]=useState(true);
    const router=useRouter();
    const authCtx=useContext(AuthContext);
    const [books,setBooks]=useState([]);
    const [page,setPage]=useState(1);
    useEffect(() => {
        const fetchBooks=async()=>{
            try{
            const response=await axios.get('/api/book');
            const data=response.data;
            setBooks(data.books.rows);
            }
            catch(err){
                console.log(err);
            }
            finally{
                setLoading(false);
            }
        }
        fetchBooks();
    }, [])

    const orderBookHandler=async(item)=>{
        try{
            const response=await axios.post('/api/order',{username:authCtx.localid,book:{...item}})
            alert(response.data.message)
            router.push('/orders')

        }
        catch(err){
            alert(err.response.data.message)
            console.log(err)
        }
                                
    }

    useEffect(() => {
        if(page>1){
        setLoading(true);
        const timer=setTimeout(() => {     
            let temp=books;
            setBooks((prevState)=>[...prevState,...temp])
            setLoading(false);
        }, 2000);
    }
    }, [page])
    

    useEffect(() => {
        const handleScroll = () => {
          const { scrollTop, clientHeight, scrollHeight } = document.documentElement;
    
          if (!loading && scrollTop + clientHeight >= scrollHeight * 0.8) {
            setPage(page+1)
          }
        };
    
        window.addEventListener('scroll', handleScroll);
        return () => {
          window.removeEventListener('scroll', handleScroll);
        };
      }, [loading]);
    
  return (
    <div className=''>
        <Header/>
        <div className='my-10 p-4'>
        <div className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-2'>
            {books.map((item,i)=>
            <div key={i} className='col-span-1 bg-white shadow-xl  rounded-xl'>
                <Image alt='book cover' width={400} height={600} src={item.cover_image}/>
                <div key={i} className='p-4 flex flex-col gap-2'>
                    <h4 className='text-lg font-bold'>{item.title}</h4>
                    <h4 className='text-lg font-bold'>{item.writer}</h4>
                    <h4 className='text-lg font-bold text-purple-500'>{item.price}</h4>
                    <div className='flex'>
                    {item.tags.map((item2,j)=><h4 key={j} className='rounded-xl bg-purple-500 text-white flex p-1'>#{item2}</h4>)}
                    </div>
                    <button onClick={()=>{
                        if(!authCtx.isLoggedIn){
                            router.push('/login')
                        }
                        else{
                            orderBookHandler(item);
                        }
                    }} className='bg-green-500 w-full text-white text-center p-2 hover:opacity-75'>BUY NOW</button>
                </div>
            </div>
            )}
        </div>
            {loading && <h4 className='text-center'>LOADING</h4>}
        </div>
    </div>
  )
}
