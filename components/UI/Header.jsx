import React, { useContext, useEffect, useState } from 'react'
import AuthContext from '../store/AuthContext'
import Link from 'next/link';
import axios from 'axios';

const Header = () => {
    const authCtx=useContext(AuthContext);

    
  return (
        <div className='py-4 px-4 bg-orange-500 text-white flex justify-between'>
            <h4>BOOK STORE</h4>
            <div className='flex gap-2'>
                <Link className='hover:underline' href='/'>HOME</Link>
                {authCtx?.isLoggedIn && <Link className='hover:underline' href='/orders'>ORDERS</Link>}
                {!authCtx?.isLoggedIn && <Link className='hover:underline' href='/login'>LOGIN</Link>}
                {!authCtx?.isLoggedIn && <Link className='hover:underline' href='/signup'>SIGNUP</Link>}
                {authCtx?.isLoggedIn && <button onClick={()=>{authCtx.logout();alert("LOGGED OUT")}} className='hover:underline' href='/signup'>LOGOUT</button>}
            </div>

        </div>
  )
}

export default Header