import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case 'POST':
    {
        try {
            const {username,book}=req.body;

            const checkQuery = `SELECT * FROM users WHERE username = $1`;
            const checkValues = [username];
      
            const checkResult = await client.query(checkQuery, checkValues);
            const existingUser = checkResult.rows[0];


            const newCredit = (+existingUser.credit) + (+book.price);
            let temp=[...existingUser.orders];
            temp=temp.filter((item,i)=>item!=book.id)
    
            const updatedUser=await client.query(
              `UPDATE users SET credit = $1, orders = $2 WHERE username = $3`,
              [newCredit, temp, username]
            );

            res.status(200).json({message:"Cancelled Successfully"})
          } catch (error) {
            console.log(error)
            return res.status(500).json({ error });
          }
    }
    }
}