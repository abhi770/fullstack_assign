import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case  'GET':
    {

 
  const books = await client.sql`SELECT * FROM books;`;
  return res.status(200).json({ books });
    }

    case 'POST':
    {
        try {
            const {id,title,writer,cover_image,price,tags}=req.body;
            console.log(req.body)
            await client.sql`CREATE TABLE IF NOT EXISTS books (
                id SERIAL PRIMARY KEY,
                title VARCHAR(255) NOT NULL,
                writer VARCHAR(255) NOT NULL,
                cover_image VARCHAR(255) NOT NULL,
                price NUMERIC(10, 2) NOT NULL,
                tags VARCHAR(255)[] NOT NULL
              );`;
            await client.sql`INSERT INTO books (id,title, writer, cover_image, price, tags)
            VALUES (${id},${title}, ${writer}, ${cover_image}, ${price}, ${tags})`;

            res.status(200).json({message:"Inserted Successfully"})
          } catch (error) {
            console.log(error)
            return res.status(500).json({ error });
          }
    }
    }
}