import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case 'POST':
    {
        try {
            const {username,password}=req.body;

            const checkQuery = `SELECT * FROM users WHERE username = $1`;
            const checkValues = [username];
      
            const checkResult = await client.query(checkQuery, checkValues);
            const existingUser = checkResult.rows[0];
      
            if (!existingUser) {
              await client.end();
              res.status(409).json({ message: 'User does not exists' });
              return;
            }
            console.log(existingUser);

            if(existingUser.password!==password){
                res.status(401).json({ message: 'Incorrect password' });
                return

            }




            res.status(200).json({message:"Login Successful"})
          } catch (error) {
            return res.status(500).json({ error });
          }
    }
    }
}