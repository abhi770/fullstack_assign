import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case 'POST':
    {
        try {
            const {username}=req.body;

            const checkQuery = `SELECT * FROM users WHERE username = $1`;
            const checkValues = [username];
      
            const checkResult = await client.query(checkQuery, checkValues);
            const existingUser = checkResult.rows[0];

            res.status(200).json({message:'User fetched successfully',data:existingUser})
          } catch (error) {
            return res.status(500).json({ error });
          }
    }
    }
}