import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case 'POST':
    {
        try {
            const {firstName,lastName,username,password}=req.body;

            const checkQuery = 'SELECT * FROM users WHERE username = $1';
            const checkValues = [username];
      
            const checkResult = await client.query(checkQuery, checkValues);
            const existingUser = checkResult.rows[0];
      
            if (existingUser) {
              // User already exists
              await client.end();
              res.status(409).json({ message: 'User already exists' });
              return;
            }

            console.log(req.body)
            const credit=100;
            const orders=[];
            await client.sql`CREATE TABLE IF NOT EXISTS users (
                firstName VARCHAR(255) NOT NULL,
                lastName VARCHAR(255) NOT NULL,
                username VARCHAR(255) NOT NULL,
                password VARCHAR(255) NOT NULL,
                credit NUMERIC(10, 2) NOT NULL,
                orders VARCHAR(255)[] NOT NULL
              );`;

            await client.sql`INSERT INTO users (firstName,lastName, username, password, credit, orders)
            VALUES (${firstName},${lastName}, ${username}, ${password},${credit}, ${orders})`;

            res.status(200).json({message:"Signup Successful"})
          } catch (error) {
            return res.status(500).json({ error });
          }
    }
    }
}