import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case 'POST':
    {
        try {
            const {username,book}=req.body;

            const checkQuery = `SELECT * FROM users WHERE username = $1`;
            const checkValues = [username];
      
            const checkResult = await client.query(checkQuery, checkValues);
            const existingUser = checkResult.rows[0];

            console.log(book);

            if((+existingUser.credit)<=(+book.price)){
                res.status(401).json({ message: 'Not enought credit' });
                return
            }
            console.log(book.price);
            console.log(existingUser.credit);

           
            const id=Math.random();

            await client.sql`CREATE TABLE IF NOT EXISTS orders (
              id VARCHAR(255) NOT NULL,
              title VARCHAR(255) NOT NULL,
              price VARCHAR(255) NOT NULL
            );`;

          await client.sql`INSERT INTO orders (id,title, price)
          VALUES (${id},${book.title}, ${book.price});`;

          const newCredit = (+existingUser.credit) - (+book.price);
          const newOrders = [...existingUser.orders,id];
    
            const updatedUser=await client.query(
              `UPDATE users SET credit = $1, orders = $2 WHERE username = $3`,
              [newCredit, newOrders, username]
            );

            console.log(updatedUser)
            console.log(newCredit);

    

            res.status(200).json({message:"Ordered Successfully",user:updatedUser.rows[0]})
          } catch (error) {
            console.log(error)
            return res.status(500).json({ error });
          }
    }
    }
}