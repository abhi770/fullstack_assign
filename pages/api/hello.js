import { db } from '@vercel/postgres';
 
export default async function handler(req, res) {
  const client = await db.connect();
    switch(req.method){
    case 'POST':
    {
        try {
            const {firstName,lastName,username,password}=req.body;
            console.log(req.body)
            await client.sql`CREATE TABLE IF NOT EXISTS books (
                firstName VARCHAR(255) NOT NULL,
                lastName VARCHAR(255) NOT NULL,
                username VARCHAR(255) NOT NULL,
                password VARCHAR(255) NOT NULL,
                credit NUMERIC(10, 2) NOT NULL,
                orders VARCHAR(255)[] NOT NULL
              );`;
            await client.sql`INSERT INTO books (firstName,lastName, username, password, credit, orders)
            VALUES (${firstName},${lastName}, ${username}, ${password},100, [])`;

            res.status(200).json({message:"Created Successfully"})
          } catch (error) {
            console.log(error)
            return res.status(500).json({ error });
          }
    }
    }
}